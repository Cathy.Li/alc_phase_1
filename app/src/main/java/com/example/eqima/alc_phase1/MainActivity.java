package com.example.eqima.alc_phase1;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button aboutALC, profileALC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("ALC 4 Phase 1");

        aboutALC = (Button)findViewById(R.id.aboutalc);
        profileALC = (Button)findViewById(R.id.profile);

        profileALC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent p = new Intent(MainActivity.this, My_Profile.class);
                startActivity(p);
            }
        });

        aboutALC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, About_ALC.class);
                startActivity(i);
            }
        });
    }
}
