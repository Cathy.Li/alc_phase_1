package com.example.eqima.alc_phase1;

import android.app.ProgressDialog;
import android.net.http.SslError;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class About_ALC extends AppCompatActivity {

    WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about__alc);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("About ALC");


        web = (WebView)findViewById(R.id.webview);

        final ProgressDialog progressBar = new ProgressDialog(About_ALC.this);
        progressBar.setMessage("Please wait ...");

        web.setWebViewClient(new WebViewClient());


        web.loadUrl("https://andela.com/alc/");


        web.getSettings().setUseWideViewPort(true);
        web.getSettings().setLoadWithOverviewMode(true);

        web.setWebViewClient(new WebViewClient(){

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
                handler.proceed();
            }
        });


    }

    public void onBackPressed(){
        if(web.canGoBack()){
            web.goBack();
        }else {
            super.onBackPressed();
        }
        super.onBackPressed();
    }
}
